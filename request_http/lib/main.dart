import 'package:flutter/material.dart';
import 'package:request_http/screens/login_screen.dart';
import 'package:request_http/screens/pin_screen.dart';
import 'package:request_http/storage/SharedPreferences.dart';
import 'package:request_http/utils/Provider.dart';

void main() async {
  runApp(MyApp());

  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Unipagos - Registro',
        initialRoute: 'login',
        routes: {
          'login'    : ( BuildContext context ) => LoginScreen(),
          'pin'    : ( BuildContext context ) => PinScreen(),
        },
        theme: ThemeData(
          primaryColor: Colors.black,
          accentColor: Colors.deepOrangeAccent
        ),
      ),
    );

  }
}
