


import 'package:flutter/material.dart';

void showAlert(BuildContext ctx,String message){
  showDialog(context: ctx,
  builder: (ctx){
    return AlertDialog(
      title: Text("¡Atención!"),
      content: Text(message),
      actions: [
        FlatButton(
            onPressed: () => Navigator.of(ctx).pop()
            , child: Text("Aceptar"))
      ],
    );
  }
  );
}