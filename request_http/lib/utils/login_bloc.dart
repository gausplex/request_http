import 'dart:async';
import 'package:request_http/utils/validators.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc with Validators {

  final _emailController    = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  final _phoneController    = BehaviorSubject<String>();
  // Recuperar los datos del Stream
  Stream<String> get emailStream    => _emailController.stream.transform(validarNumber);
  Stream<String> get phoneStream    => _phoneController.stream.transform(validarNumber);


  // Insertar valores al Stream
  Function(String) get changeEmail    => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;
  Function(String) get changePhone    => _phoneController.sink.add;


  // Obtener el último valor ingresado a los streams
  String get email    => _emailController.value;
  String get password => _passwordController.value;
  String get phone    => _phoneController.value;

  dispose() {
    _emailController?.close();
    _passwordController?.close();
  }

}

