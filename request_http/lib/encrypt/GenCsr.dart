import 'dart:io';
// import "package:x509csr/x509csr.dart";
import 'package:flutter/services.dart';
import "package:pointycastle/export.dart";
// import 'package:asn1lib/asn1lib.dart';
/*import 'package:basic_utils/basic_utils.dart';

import 'package:basic_utils/src/model/x509/X509CertificateData.dart';
import 'package:basic_utils/src/model/x509/X509CertificatePublicKeyData.dart';
import 'package:basic_utils/src/model/x509/X509CertificateValidity.dart';

import 'package:pointycastle/export.dart';
import 'package:pointycastle/asn1/primitives/asn1_integer.dart';
import 'package:pointycastle/asn1/primitives/asn1_bit_string.dart';
import 'package:pointycastle/asn1/primitives/asn1_sequence.dart';
import 'package:pointycastle/asn1/primitives/asn1_null.dart';
import 'package:pointycastle/asn1/primitives/asn1_object_identifier.dart';
import 'package:pointycastle/asn1/asn1_object.dart';
import 'package:pointycastle/asn1/asn1_tags.dart';
import 'package:pointycastle/asn1/primitives/asn1_utf8_string.dart';
import 'package:pointycastle/asn1/primitives/asn1_set.dart';
import 'package:pointycastle/asn1/primitives/asn1_printable_string.dart';*/

import 'package:pointycastle/asymmetric/api.dart';

import 'package:x509csr/x509csr.dart';

import 'package:pointycastle/export.dart';
import 'package:asn1lib/asn1lib.dart';

// import 'dart:math';
// import 'dart:typed_data';
// import "package:pointycastle/pointycastle.dart";
import 'dart:convert';








String generateCrs(){


AsymmetricKeyPair keyPair = rsaGenerateKeyPair();
 
ASN1ObjectIdentifier.registerFrequentNames();
  Map<String, String> dn = {
    "CN": "www.davidjanes.com",
    "O": "Consensas",
    "L": "Toronto",
    "ST": "Ontario",
    "C": "CA",
  };

  ASN1Object encodedCSR = makeRSACSR(dn, keyPair.privateKey, keyPair.publicKey);

  final salidaCSR = encodeCSRToPem(encodedCSR);

  print('salida  : $salidaCSR');

  print(encodeCSRToPem(encodedCSR));
  //print(encodeRSAPublicKeyToPem(keyPair.publicKey));
  //print(encodeRSAPrivateKeyToPem(keyPair.privateKey));

  //AsymmetricKeyPair keyPair = CryptoUtils.generateEcKeyPair();

  print('keyPair : $keyPair');

  //Map<String, String> dn = {
  //  "UID":   number
  //};

  print('keyPair.privateKey : $keyPair.privateKey');

  ///String csr = CryptoUtil.generateEccCsrPem(dn, keyPair.privateKey, keyPair.publicKey);

  print('encodedCSR : $encodedCSR');
  //csr = base64CRS(encodedCSR);

  return  salidaCSR;
}

String base64CRS(String crs){
  String csrBase64;

print('csrBase64: $csrBase64');
csrBase64 = ("-----BEGIN CERTIFICATE REQUEST-----"  '\n' + crs + '\n' + "-----END CERTIFICATE REQUEST-----");
 // csrBase64 = csrBase64.replaceAll("-----END CERTIFICATE REQUEST-----", "");
 // csrBase64 = csrBase64.replaceAll("-----BEGIN CERTIFICATE REQUEST-----", "");
 // csrBase64 = csrBase64.replaceAll("\r", "");
 // csrBase64 = csrBase64.replaceAll("\n", "");
  print('csrBase64 : $csrBase64');
  return csrBase64;
}

  /*class CryptoUtil {

  static String generateEccCsrPem(Map<String, String> attributes,
      ECPrivateKey privateKey, ECPublicKey publicKey) {

    print('privateKey  : $privateKey');
    print('publicKey  : $publicKey');
    
    var encodedDN = encodeDN(attributes);
    var publicKeySequence = _makeEccPublicKeyBlock(publicKey);

    var blockDN = ASN1Sequence();
    blockDN.add(ASN1Integer(BigInt.from(0)));
    blockDN.add(encodedDN);
    blockDN.add(publicKeySequence);
    blockDN.add(ASN1Null(tag: 0xA0)); // let's call this WTF

    var blockSignatureAlgorithm = ASN1Sequence();
    blockSignatureAlgorithm
        .add(ASN1ObjectIdentifier.fromName('ecdsaWithSHA256'));

    var ecSignature = X509Utils.eccSign(blockDN.encode(), privateKey);

    var bitStringSequence = ASN1Sequence();
    bitStringSequence.add(ASN1Integer(ecSignature.r));
    bitStringSequence.add(ASN1Integer(ecSignature.s));
    var blockSignatureValue =
    ASN1BitString(stringValues: bitStringSequence.encode());

    var outer = ASN1Sequence();
    outer.add(blockDN);
    outer.add(blockSignatureAlgorithm);
    outer.add(blockSignatureValue);
    var chunks = StringUtils.chunk(base64.encode(outer.encode()), 64);
    return '${chunks.join('\r\n')}';
  }

  static ASN1Sequence _makeEccPublicKeyBlock(ECPublicKey publicKey) {
    var algorithm = ASN1Sequence();
    algorithm.add(ASN1ObjectIdentifier.fromName('ecPublicKey'));
    algorithm
        .add(ASN1ObjectIdentifier.fromName(publicKey.parameters.domainName));

    var subjectPublicKey =
    ASN1BitString(stringValues: publicKey.Q.getEncoded(false));

    var outer = ASN1Sequence();
    outer.add(algorithm);
    outer.add(subjectPublicKey);

    return outer;
  }

  static ASN1Object encodeDN(Map<String, String> dn) {
    var distinguishedName = ASN1Sequence();
    dn.forEach((name, value) {
      var oid = UIDASN1ObjectIdentifier();
      if (oid == null) {
        throw ArgumentError('Unknown distinguished name field $name');
      }

      ASN1Object ovalue;
      switch (name.toUpperCase()) {
        case 'C':
          ovalue = ASN1PrintableString(stringValue: value);
          break;
        case 'CN':
        case 'O':
        case 'L':
        case 'S':
        default:
          ovalue = ASN1UTF8String(utf8StringValue: value);
          break;
      }

      if (ovalue == null) {
        throw ArgumentError('Could not process distinguished name field $name');
      }

      var pair = ASN1Sequence();
      pair.add(oid);
      pair.add(ovalue);

      var pairset = ASN1Set();
      pairset.add(pair);

      distinguishedName.add(pairset);
    });

    return distinguishedName;
  }


} */
/*class UIDASN1ObjectIdentifier extends ASN1ObjectIdentifier {
  UIDASN1ObjectIdentifier() : super.fromName('organizationName') {
    objectIdentifierAsString = '0.9.2342.19200300.100.1.1';
    readableName = 'UID';
    objectIdentifier = [0, 9, 2342, 19200300, 100, 1, 1];
  } 

}*/